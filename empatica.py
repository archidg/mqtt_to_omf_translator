# Import packages
import sys, json, random, time, platform, requests, socket, datetime, random

relay_url = "https://128.173.237.85:5460/ingress/messages"
producer_token = "Empatica-6"

value = 10
# ************************************************************************
def create_data_values_stream_message(target_stream_id,Datavalue):
    data_values_JSON = ''
    timestamp = datetime.datetime.utcnow().isoformat() + 'Z'
    data_values_JSON = [
        {
            "containerid": target_stream_id,
            "values": [
                {
                    "Time": timestamp,
                    "reading1": Datavalue
                }
            ]
        }
    ]
    return data_values_JSON

# ************************************************************************
def sendOMFMessageToEndPoint(message_type, OMF_data):
        try: 
                msg_header = {'producertoken': producer_token, 'messagetype': message_type, 'action': 'create', 'messageformat': 'JSON', 'omfversion': '1.0'}   
                response = requests.post(relay_url, headers=msg_header, data=json.dumps(OMF_data), verify=False, timeout=30)
                print('Response from relay from the initial "{0}" message: {1} {2}'.format(message_type, response.status_code, response.text))
        except Exception as e:
                print(str(datetime.datetime.now()) + " An error ocurred during web request: " + str(e))		

# ************************************************************************

types = [
    {
        "id": "type_empatica",  #type_tank
        "type": "object",
        "classification": "static",
        "properties": {
                "Name": {
                        "type": "string",
                        "isindex": True
                },
                "Location": {
                        "type": "string"
                }
        }
	},
	{
        "id": "type_measurement",
        "type": "object",
        "classification": "dynamic",
        "properties": {
                "Time": {
                        "format": "date-time",
                        "type": "string",
                        "isindex": True
                },
                "reading1": {
                        "type": "number"
                }
        }
    }
]

containers = [{
        "id": "measurement1",
        "typeid": "type_measurement"
}]

staticData = [{
        "typeid": "type_empatica",   #type_tank
        "values": [{
                "Name": "BVP",          #tank1
                "Location": "New York"
        }]
},
{
        "typeid": "type_empatica",   #type_tank
        "values": [{
                "Name": "BVP",
                "Location": "New York"
        }]
}]

linkData = [{
        "typeid": "__Link",
        "values": [ {
                "source": {
                        "typeid": "type_empatica",
                        "index": "_ROOT"
                },
                "target": {
                        "typeid": "type_empatica",
                        "index": "BVP"
                }
        },{
                "source": {
                        "typeid": "type_empatica",
                        "index": "BVP"
                },
                "target": {
                        "containerid": "measurement1"
                }
        }]
    }]

#dataValue = [{
#        "containerid": "measurement1",
#        "values": [{
#                "Time": "2017-01-11T22:23:23.430Z",
#                "reading1": "1.0",
#                "reading2": "2.0"
#        }]
#}]

requests.packages.urllib3.disable_warnings()

    
sendOMFMessageToEndPoint("Type", types)
#time.sleep(1)
sendOMFMessageToEndPoint("Container", containers)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", staticData)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", linkData)
# time.sleep(1)
# ************************************************************************
while True:
    values = create_data_values_stream_message("measurement1",value)
    sendOMFMessageToEndPoint("Data", values)
  #  time.sleep(1)
