# This is a Connector that receives mqtt messages and then translates them into the OMF(OSIsoft Message Format)
# to store into a PI System. This client subscribes to different topics published on an MQTT server.
# Once connected, it uses the call back functions to consume data from the subscribed topics and to create 
# appropriate OMF message types. Then the data is formatted accordingly and sent to the pi server using the relay.
# An AF hierarchy is created based on the topic. Each element and container has its own unique ID. Containers can 
# receive both float/int and string type data.
 
# Imports packages (for producing OMF messages)
import sys, json, random, time, platform, requests, socket, datetime, random
import logging, paho.mqtt.client as mqtt

dictionary_semantics = dict()
containers_dict = dict()
static_element_dict = dict()
type_version_num = "1.0.0.0"

# Opens a config file named config.json. This is hard coded as of now, not sure if it needs to made turned into user input
# Error handling exists if the program can't open config.json
if __name__ == "__main__":
    try:
       f = open('config.json', 'rb')
    except IOError:
        print("Could not read file: {0}\n". format('config.json'))
        sys.exit(2)
    with open('config.json') as json_data_file:
        config_data = json.load(json_data_file) # parsed as a python dictionary

    print("Read config file data:\n{data}\n".format(data=json.dumps(config_data, sort_keys=True, indent=4)))
    
    # Here we use values in the python dictionary to set the values for the remainder of the program
    try:
        producerToken_key = "producerToken"
        producer_token = config_data[producerToken_key]
    except KeyError as kex:
        logging.critical("Unable to find key {0} in config.json, exiting program!\n".format(producerToken_key))
        sys.exit(2)
    print("Producer token has been configured to: {0}".format(producer_token))
    try:
        relayURL_key = "relayURL"
        relay_url = config_data[relayURL_key]
    except KeyError as kex:
        logging.critical("Unable to find URL {0} in config.json, exiting program!\n".format(relayURL_key))
        sys.exit(2)
    print("Relay URL has been configured to: {0}".format(relay_url))
    try:
        MQTT_broker_key = "mqtt_broker_url"
        broker_address = config_data[MQTT_broker_key]
    except KeyError as kex:
        logging.critical("Unable to find address {0} in config.json, exiting program!\n".format(MQTT_broker_key))
        sys.exit(2)
    print("MQTT address has been configured to: {0}".format(broker_address))
    try:
        MQTT_topic_key = "mqtt_topic_filter"
        MQTT_topic_list = config_data[MQTT_topic_key].split(',') 
    except KeyError as kex:
        logging.critical("Unable to find topic {0} in config.json, exiting program!\n".format(MQTT_topic_key))
        sys.exit(2)
    print("MQTT topic has been configured to: {0}".format(MQTT_topic_list))
    try:
        logfileName_key = "log_file_name"
        logfileName = config_data[logfileName_key]
    except KeyError as kex:
        logging.critical("Unable to find logfileName {0} in config.json, exiting program!\n".format(logfileName_key))
        sys.exit(2)
    print("Log file name has been configured to: {0}\n".format(logfileName))
#****************************************************************************************************************

def on_connect(client, userdata, flags, rc):
    """
    Function: on_connect
    When a MQTT client is created and connected to a broker_address specified in the .confg file correctly,
    the on_connect callback is called by the paho.mqtt framework after first connecting to a mqtt broker. 
      Args:
          client: the client instance for this callback
          userdata: the private user data set in mqtt client
          flags: response flags sent by the broker
          rc: the connection result. More about the numbers can be found on MQTT documentation
    """

    logging.basicConfig(filename=logfileName,level=logging.INFO)
    print("\nConnected flags: {0}, result code: {1}\n".format(flags, rc))


def on_message(client1, userdata, message):
    """
    When a MQTT client receives a message from a subscribed topic, this call back is invoked by paho.mqtt framework.
    It creates and sends OMF type declarations to the connector relay, so that they exist before
    sending data. It creates unique static elements only once for each relevant part of the mqtt message topic hierarchy and stores
    them in a dictionary. Then, it creates and sends link data for the static data to form an AF hierarchy. An unique
    container is created only once for the last element of an unique topic and stored into the dictionary. Then, 
    a link data is created and sent to the relay. The message is finally translated and formatted into an OMF message,
    and is sent to the PI relay.
    Args:
       client1: the client instance for this callback
       userdata: the private user data set in mqtt client
       message: the message received by the client, contains various information such as the topic, payload, QoS
    """
    global dictionary_semantics
    try:
       decodeTest = message.payload.decode("utf-8")
    except UnicodeDecodeError as ex:
        print("New MQTT Message received: {0}, the payload is an illegal payload. The program will reject the message.\n".format(message.payload.decode("utf-8")))
    else:
        # Check if it's 'Semantics' or 'Data' Message 
        messageCategory = message.topic.split("/",1)[0]   
        print("New MQTT Message received. Category: {0}".format(messageCategory))
        
        # Read the semantic data   
        if messageCategory == "Semantics" :
            dataTopic = message.topic.split("/",1)[1] 
            dictionary_semantics[dataTopic] = json.loads(message.payload) # parsed as a python dictionary
            print("dataTopic: {0}, \npayload: {1}\n".format(dataTopic, json.dumps(json.loads(message.payload), indent = 4)))
        
        # Create AF Hierarchy with Data Message    
        else:  # if messageCategory == "Data" :
            messageTopic = message.topic.split("/",1)[1]
            topicElementsList = messageTopic.split("/") 
            lastElement = topicElementsList[-1] 
            print("Payload: {0}, message topic: {1}\n".format(message.payload.decode("utf-8"), messageTopic))
            logging.info("Payload: {0}, last element: {1}\n".format(message.payload.decode("utf-8"), lastElement))

            # Create and send Type Messages, Data Messages (Static Data & Link Data) and Container Messages
            for index in range (len(topicElementsList)):
                # Ensure unique name for all Static elements  
                if index == 0:
                    topicElement = topicElementsList[index]
                else:
                    topicElementPrev = topicElement
                    topicElement = topicElement + "/" + topicElementsList[index]

                # TYPE MESSAGE & STATIC DATA: use dictionary to ensure Type Message & static data is created only once for each unique element
                if topicElement in static_element_dict:
                    print("STATIC DATA and TYPE MESSAGE already in dictionary & endpoint for: {0}\n".format(topicElement)) 
                    logging.info("STATIC DATA and TYPE MESSAGE already in dictionary & endpoint for: {0}\n ".format(topicElement)) 
                else:                                           
                    if index == (len(topicElementsList) - 1): 
                        payloadType = dictionary_semantics[topicElement]["Reading Type"]                         #@#@#@#@

                        print("Semantic dictionary: {0}\n".format(json.dumps(dictionary_semantics, indent = 4)))
                        attribute_dict = dictionary_semantics[topicElement]        
                       
                        # Create type message for each leaf node by providing unique typeID, using the topicElement name
                        typeID =  "type_lastElement_" + topicElement 
                        types_lastElement = create_type_message_lastElement(typeID)   
                       
                        # Add the attribute dictionary to the value of "properties" of this list of dictionaries, "types_lastElement"  
                        for key in attribute_dict:
                           types_lastElement[0]["properties"][key] = {"type" : "string"}
                        
                        sendOMFMessageToEndPoint("Type", types_lastElement)
                        print("Created & Sent TYPE MESSAGE for last element: {0}, typeID: {1}\n, type message: {2}\n".format(topicElement, typeID, json.dumps(types_lastElement, indent = 4)))
                        
                        name = topicElement 
                        staticData = create_static_data_lastElement(typeID, name)   

                        # Add the attribute dictionary to the value of "values" in this list of dictionaries, "staticData"
                        for key in attribute_dict:
                           staticData[0]["values"][0][key] = attribute_dict[key]
                        print("staticData: {0}\n".format(json.dumps(staticData, indent = 4)))
                        
                    else:
                        # Create and send Type Message and static data for other elements
                        typeID = "typ_ELement"
                        location_key = "location"   
                        create_type_message_element(typeID, location_key)
                        print("Created & Sent TYPE MESSAGE for current element: {0}\n".format(topicElement))                 
                        location_value = topicElement   
                        name = topicElement
                        staticData = create_static_data_element(typeID, name, location_key, location_value)
                    sendOMFMessageToEndPoint("Data", staticData)
                    print("Created & Sent STATIC DATA for current element: {0}\n".format(topicElement))
                    logging.info("Created & Sent STATIC DATA for current element: {0}\n".format(topicElement))
                    static_element_dict[topicElement] = staticData
        
                # LINK DATA: to create the AF hierarchy in PI system with Static data         
                if index == 0:   
                    sourceIndex = "_ROOT"
                    typeID = "typ_ELement"
                    targetIndex = topicElement
                    linkData_Element = create_link_data_elements(sourceIndex, typeID, targetIndex)    
                    sendOMFMessageToEndPoint("Data", linkData_Element)
                    print("Sent LINK DATA for first element: {0}\n".format(topicElement))
                    logging.info("Sent LINK DATA for first element: {0}\n".format(topicElement))
                    topicElementPrev = topicElement  # Redundant? 
                elif index == (len(topicElementsList) - 1):
                    payloadType = dictionary_semantics[topicElement]["Reading Type"]  
                    sourceIndex = topicElementPrev
                    typeID =  "type_lastElement_" + topicElement                    
                    targetIndex = topicElement
                    linkData_Element = create_link_data_elements(sourceIndex, typeID, targetIndex)    
                    sendOMFMessageToEndPoint("Data", linkData_Element)
                    print("Sent LINK DATA for last element: {0}\n".format(topicElement))
                    logging.info("Sent LINK DATA for last element: {0}\n".format(topicElement))
                    topicElementPrev = topicElement

                    # CONTAINER MESSAGE: create and link container values to last element
                    # Use dictionary to avoid creating conatiner for same element multiple times  
                    container_id = "none"                  
                    if container_id in containers_dict :
                        print("ContainerID already present in dictionary & endpoint. container id: {0}\n".format(container_id))
                        logging.info("ContainerID already present in dictionary & endpoint. container id: {0}\n".format(container_id))
                    else:
                        # Create container id for last elements; create and send type messages for containers
                        container_id = topicElementPrev + "_Readings" ###  + payloadType                               #@#@#@#@#@#@ 
                        typeID = lastElement + "_Reading" ###  + payloadType               #@#@#@#@#@#@
                        if payloadType == "float":
                            payLoad = float(message.payload)
                            readingType = "number"  
                        else:
                            payLoad = message.payload.decode(encoding = "utf-8")
                            readingType = "string"

                        types = create_type_message_container(typeID, readingType)   
                        sendOMFMessageToEndPoint("Type", types)
                        print("Created & Sent Dynamic TYPE MESSAGE for container for last element: {0},\nContainer Type Message: {1}\n".format(topicElement, json.dumps(types, indent = 4)))     
                        containers = create_container_message(container_id, typeID)  
                        sendOMFMessageToEndPoint("Container", containers)
                        print("New CONTAINER message created and sent with container_id: {0}, container message: {1}\n".format(container_id, json.dumps(containers, indent = 4)))
                        logging.info("New CONTAINER message created and sent with container_id: {0}\n".format(container_id))  

                        values = create_data_values_stream_message(typeID, container_id, payLoad)   
                        sendOMFMessageToEndPoint("Data", values)
                        logging.info("Data values created & sent for container id: {0}, values: {1}\n".format(container_id, json.dumps(values, indent = 4)))
                        print("Data values created & sent for container id: {0}, values: {1}\n".format(container_id, json.dumps(values, indent = 4)))
                        containers_dict[container_id] = containers  
                    
                    # Link the container to the last element/ leaf node
                    typeID = "type_lastElement_" + topicElement 
                    sourceIndex = topicElementPrev
                    linkData_Container = create_link_data_container(typeID, sourceIndex, container_id)    
                    sendOMFMessageToEndPoint("Data", linkData_Container)
                    print("Container linked to last element: {0}, linkData_Container: {1}\n".format(topicElementPrev, json.dumps(linkData_Container, indent = 4)))
                    logging.info("Container linked to last element: {0}, linkData_Container: {1}\n".format(topicElementPrev, json.dumps(linkData_Container, indent = 4)))  
                                
                else:
                    sourceIndex = topicElementPrev
                    typeID = "typ_ELement"
                    targetIndex = topicElement
                    linkData_Element = create_link_data_elements(sourceIndex, typeID, targetIndex)    
                    sendOMFMessageToEndPoint("Data", linkData_Element)
                    print("Sent LINK DATA for current element: {0}\n".format(topicElement))
                    logging.info("Sent LINK DATA for current element: {0}\n".format(topicElement))     
                    topicElementPrev = topicElement    # Redundant?
# ************************************************************************************************************
# TYPE MESSAGES
# Create and send OMF type declarations for the elements to the connector relay.
def create_type_message_element(typeID, location_key):
    if typeID == "typ_ELement":
        types = [{
            "id": typeID,
            "version" : type_version_num,
            "type": "object",
            "classification": "static",
            "properties": {
                    "Name": {
                            "type": "string",
                            "isindex": True
                    },
                    location_key: {
                            "type": "string"
                    }
            }
	    }]
    sendOMFMessageToEndPoint("Type", types)
    

# Create and send OMF type declarations for the elements to the connector relay.
def create_type_message_lastElement(typeID):       
    types_lastElement = [{                          #@#@#@
                            "id": typeID,
                            "version" : type_version_num,
                            "type": "object",
                            "classification": "static",
                            "properties": {
                                    "Name": {
                                            "type": "string",
                                            "isindex": True
                                            }
                                         }
	                        }]
    return types_lastElement


# Create and send OMF type declarations for the containers to the connector relay.
def create_type_message_container(typeID, readingType):  
    types = [{
        "id": typeID,
        "version" : type_version_num,
        "type": "object",
        "classification": "dynamic",
        "properties": {
                "Time": {
                        "format": "date-time",
                        "type": "string",
                        "isindex": True
                },
                "Reading": {
                        "type": readingType
                }
        }
    }]
    return types
    
# ************************************************************************************************************
# STATIC DATA
# Creates static data and sends to end point
def create_static_data_element(typeID, name, location_key, location_value):
    typeid = "typ_ELement"
    staticData = [{
                  "typeid": typeid,
                  "typeversion" : type_version_num,
                  "values": [{
                            "Name": name,  
                            location_key: location_value
                    }]
                }]
    return staticData


# Creates static data and sends to end point
def create_static_data_lastElement(typeID, name):               #@#@#@
    staticData = [{
        "typeid": typeID,
        "typeversion" : type_version_num,
        "values": [{
            "Name": name  
                  }]
    }]
    return staticData
# ************************************************************************************************************
# CONATINER MESSAGE
# Creates Containers according to string or float data
def create_container_message(ContainerID, typeID):
    container1 = ''
    container1 = [{
                     "id": ContainerID,
                     "typeid": typeID,
                     "typeVersion": type_version_num
                 }]
    return container1

# ************************************************************************************************************
# LINK DATA
#Create LinkData for elements
def create_link_data_elements(SourceIndex, typeID, TargetIndex):
    linkData1_Element = [{
                "typeid": "__Link",
                "values": [{
                        "source": {
                                "typeid": "typ_ELement",
                                "typeversion" : type_version_num,
                                "index": SourceIndex
                        },
                        "target": {
                                "typeid": typeID,
                                "typeversion" : type_version_num,
                                "index": TargetIndex    
                        }
                }]
    }]
    return linkData1_Element


#Create LinkData for containers
def create_link_data_container(typeID, Index, ContainerID):
    linkData2_Container = [{
                    "typeid": "__Link",
                    "values": [{
                            "source": {
                                "typeid": typeID,
                                "typeversion" : type_version_num,
                                "index": Index   
                            },
                            "target": {
                                "containerid": ContainerID
                            }
                    }]
            }]
    return linkData2_Container
# ************************************************************************************************************
# Data Message
# Creates data message, distinguishes between float / string type
def create_data_values_stream_message(typeID, containerID, payLoad):   
    data_values_JSON = ''
    timestamp = datetime.datetime.utcnow().isoformat() + 'Z'
    data_values_JSON = [
          {
            #####"typeid": typeID,
            "containerid": containerID,
            "typeversion" : type_version_num,
            "values": [
                {
                    "Time": timestamp,
                    "Reading": payLoad
                }
            ]
        }
    ]
    return data_values_JSON
# ************************************************************************************************************

#Sends formatted message to the PI relay
def sendOMFMessageToEndPoint(message_type, OMF_data):
        try: 
                msg_header = {'producertoken': producer_token, 'messagetype': message_type, 'action': 'create', 'messageformat': 'JSON', 'omfversion': '1.0'}   
                response = requests.post(relay_url, headers=msg_header, data=json.dumps(OMF_data, indent = 4), verify=False, timeout=30)
                print('Response from relay from the initial "{0}" message: {1} {2}'.format(message_type, response.status_code, response.text))
                logging.info('Response from relay from the initial "{0}" message: {1} {2}'.format(message_type, response.status_code, response.text))
                if response.status_code == 400:
                    print(OMF_data)
        except Exception as e:
                print(str(datetime.datetime.now()) + " An error ocurred during web request: " + str(e))		
# ************************************************************************************************************
# Create an MQTT client that would subscribe to the required topics 


while True:

	client1 = mqtt.Client("P1")  
	print("MQTT client 'P1' created.\n")

	client1.username_pw_set("futurehaus", "HokieDVE")
	print("username, password set\n")

	client1.on_connect= on_connect
	client1.on_message=on_message        
	#time.sleep(0.25)
	client1.connect(broker_address)      
	client1.loop_start()
	for n in MQTT_topic_list:
		client1.subscribe(n)
		time.sleep(0.01)
		client1.unsubscribe(n)
        

	input("Press any key to continue...")

 
	#time.sleep(0.25)