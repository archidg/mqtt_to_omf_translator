# Import packages
import sys, json, random, time, platform, requests, socket, datetime, random
import paho.mqtt.client as mqtt

#%%

relay_url = "https://128.173.237.85:5460/ingress/messages"
producer_token = "Empatica-12"
output = 0
broker_address = "127.0.0.1"
connected = False

#f = open('log.txt','w')

#%%
def on_connect(client, userdata, flags, rc):
    """
    Function: on_connect
    When a MQTT client is created and connected to a broker_address specified in the .confg file correctly,
    the on_connect callback is called by the paho.mqtt framework after first connecting to a mqtt broker. 
      Args:
          client: the client instance for this callback
          userdata: the private user data set in mqtt client
          flags: response flags sent by the broker
          rc: the connection result. More about the numbers can be found on MQTT documentation
    """

    if rc == 0:
        global connected
        connected = True
       # f.open('log.txt','rw')
        print("\nConnected flags: {0}, result code: {1}\n".format(flags, rc))
    else:
        print("Connection failed")
        client.loop_stop()
#%%
def on_message(client1, userdata, message):
    """
    When a MQTT client receives a message from a subscribed topic, this call back is invoked by paho.mqtt framework.
    It creates and sends OMF type declarations to the connector relay, so that they exist before
    sending data. It creates unique static elements only once for each relevant part of the mqtt message topic hierarchy and stores
    them in a dictionary. Then, it creates and sends link data for the static data to form an AF hierarchy. An unique
    container is created only once for the last element of an unique topic and stored into the dictionary. Then, 
    a link data is created and sent to the relay. The message is finally translated and formatted into an OMF message,
    and is sent to the PI relay.
    Args:
       client1: the client instance for this callback
       userdata: the private user data set in mqtt client
       message: the message received by the client, contains various information such as the topic, payload, QoS
    """
    global output
    topic = message.topic.split("/",1)[-1]
    if (topic == "Empatica/BD2CBC/BVP"):
        output = float(message.payload.decode("utf-8"))
        ##
        values = create_data_values_stream_message("measurement1",output)
        #print("BVP_Timestamp: " + datetime.datetime.utcnow().isoformat() + 'Z')
#        logging.info("BVP timestamp: {0}".format("logging"))
#        f.write("BVP_Timestamp: " + format(datetime.datetime.utcnow().isoformat() + 'Z')+ '\n')
        print("BVP_Timestamp: " + format(datetime.datetime.utcnow().isoformat() + 'Z')+ '\n')
        sendOMFMessageToEndPoint("Data", values)

        
    elif(topic == "Empatica/BD2CBC/GSR"):
        output = float(message.payload.decode("utf-8"))
        values = create_data_values_stream_message("measurement2",output)
        #print("GSR_Timestamp: " + datetime.datetime.utcnow().isoformat() + 'Z')
#        f.write("GSR timestamp: {0}".format(datetime.datetime.utcnow().isoformat() + 'Z')+ '\n')
        print("GSR timestamp: {0}".format(datetime.datetime.utcnow().isoformat() + 'Z')+ '\n')
        sendOMFMessageToEndPoint("Data", values)
        

        
#%%
# ************************************************************************
def create_data_values_stream_message(target_stream_id, value):
    data_values_JSON = ''
    timestamp = datetime.datetime.utcnow().isoformat() + 'Z'
    data_values_JSON = [
        {
            "containerid": target_stream_id,
            "values": [
                {
                    "Time": timestamp,
                    "reading1": value,
                    "reading2": 0
                }
            ]
        }
    ]
    return data_values_JSON

# ************************************************************************
def sendOMFMessageToEndPoint(message_type, OMF_data):
        try: 
                msg_header = {'producertoken': producer_token, 'messagetype': message_type, 'action': 'create', 'messageformat': 'JSON', 'omfversion': '1.0'}   
                response = requests.post(relay_url, headers=msg_header, data=json.dumps(OMF_data), verify=False, timeout=30)
#                print('Response from relay from the initial "{0}" message: {1} {2}'.format(message_type, response.status_code, response.text))
#                f.write("Send to EndPoint timestamp: {0}".format(datetime.datetime.utcnow().isoformat() + 'Z')+ '\n')
                print("Send to EndPoint timestamp: {0}".format(datetime.datetime.utcnow().isoformat() + 'Z')+ '\n')
        except Exception as e:
                print(str(datetime.datetime.now()) + " An error ocurred during web request: " + str(e))		

# ************************************************************************


types = [
    {
        "id": "type_empatica",  #type_tank
        "type": "object",
        "classification": "static",
        "properties": {
                "Name": {
                        "type": "string",
                        "isindex": True
                },
                "Location": {
                        "type": "string"
                }
        }
	},
	
	
	{
        "id": "type_measurement",
        "type": "object",
        "classification": "dynamic",
        "properties": {
                "Time": {
                        "format": "date-time",
                        "type": "string",
                        "isindex": True
                },
                "reading1": {
                        "type": "number"
                },
                "reading2": {
                        "type": "number"
                }
        }
    }
]

containers = [{
        "id": "measurement1",
        "typeid": "type_measurement"
},
{
        "id": "measurement2",
        "typeid": "type_measurement"
}]


staticData = [{
        "typeid": "type_empatica",   #type_tank
        "values": [{
                "Name": "BVP",          #tank1
                "Location": "New York"
        }]
},
{
        "typeid": "type_empatica",   #type_tank
        "values": [{
                "Name": "GSR",
                "Location": "New York"
        }]
}
]

linkData = [{
        "typeid": "__Link",
        "values": [ {
                "source": {
                        "typeid": "type_empatica",
                        "index": "_ROOT"
                },
                "target": {
                        "typeid": "type_empatica",
                        "index": "BVP"
                }
        },{
                "source": {
                        "typeid": "type_empatica",
                        "index": "BVP"
                },
                "target": {
                        "containerid": "measurement1"
                }
        },
        ######
        {
                "source": {
                        "typeid": "type_empatica",
                        "index": "_ROOT"
                },
                "target": {
                        "typeid": "type_empatica",
                        "index": "GSR"
                }
        },{
                "source": {
                        "typeid": "type_empatica",
                        "index": "GSR"
                },
                "target": {
                        "containerid": "measurement2"
                }
        }
        
        
        
        ]
    }]

#dataValue = [{
#        "containerid": "measurement1",
#        "values": [{
#                "Time": "2017-01-11T22:23:23.430Z",
#                "reading1": "1.0",
#                "reading2": "2.0"
#        }]
#}]

requests.packages.urllib3.disable_warnings()

    
sendOMFMessageToEndPoint("Type", types)
#time.sleep(1)
sendOMFMessageToEndPoint("Container", containers)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", staticData)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", linkData)
# time.sleep(1)
# ************************************************************************


try:
    client1 = mqtt.Client("P1")  
    #print("MQTT client 'P1' created.\n")

    client1.username_pw_set("futurehaus", "HokieDVE")
    #print("username, password set\n")
    
    client1.on_connect= on_connect
    client1.on_message=on_message
    client1.connect(broker_address)
    #client1.loop_start()
#
#    while connected != True:
#        print("Trying to connect!")
#        time.sleep(0.1)

    
   # client1.subscribe(("Data/#",0))  

    while True:   
        client1.subscribe(("Data/#",0))
        client1.loop()
        ##values = create_data_values_stream_message("measurement1",output)
        ##sendOMFMessageToEndPoint("Data", values)
  #  time.sleep(1)
  
except KeyboardInterrupt:
    client1.disconnect()
    client1.loop_stop()
#    f.close()