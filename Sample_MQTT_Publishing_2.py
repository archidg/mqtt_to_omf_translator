# This program creates an MQTT Publisher Client and publishes to the broker address enterd by the user. 

import paho.mqtt.client as mqtt
import time, random, json

print("\nPlease enter the broker address...")

broker_address = input()

client2 = mqtt.Client("2")          
#time.sleep(1)
client2.username_pw_set("futurehaus", "HokieDVE")
client2.connect(broker_address)      
client2.loop_start()    
client2.publish("Semantics/F_Haus/Floor_1/Bedroom_A/Door", json.dumps({'Description': 'Monitors if the door is open', 'Reading Type' : 'string'}))
client2.publish("Semantics/F_Haus/Floor_1/Bedroom_A/Temperature", json.dumps({'Description': 'Monitors the temperature', 'Reading Type' : 'float', 'Measurement Unit': 'Fahrenheit (F)'}))
client2.publish("Semantics/F_Haus/Floor_2/MasterBed/DeskLamp", json.dumps({'Description': 'Monitors the desk lamp', 'Reading Type' : 'string'}))
client2.publish("Semantics/F_Haus/Floor_2/MasterBed/CeilingFan", json.dumps({'Description': 'Monitors the ceiling fan', 'Reading Type' : 'string'}))
client2.publish("Semantics/F_Haus/Floor_2/MasterBed/Heating", json.dumps({'Description': 'Monitors the heating', 'Reading Type' : 'float'}))
client2.publish("Semantics/F_Haus/Floor_3/Bedroom_A/Humidity", json.dumps({'Description': 'Monitors the humidity', 'Reading Type' : 'string'}))


while True:

    #json dumps -> returns a string representing a json object from an object.
	client2.publish("Data/F_Haus/Floor_1/Bedroom_A/Door", "Open")
	client2.publish("Data/F_Haus/Floor_1/Bedroom_A/Temperature", random.uniform(10,20))
	client2.publish("Data/F_Haus/Floor_2/MasterBed/DeskLamp", "OFF")
	client2.publish("Data/F_Haus/Floor_2/MasterBed/CeilingFan", "ON")
	time.sleep(0.5)
	client2.publish("Data/F_Haus/Floor_2/MasterBed/Heating", random.uniform(30,40))  #@#@
	client2.publish("Data/F_Haus/Floor_3/Bedroom_A/Humidity", "WARNING")
	time.sleep(0.5)
	
	#client2.publish("Data/F_Haus/Floor_2/MasterBed/Heating", "ON")    #@#@

    #client2.publish("Semantics/F_Haus/Floor_1/Bedroom_A/Door", json.dumps({'Description': 'Monitors if the door is open', 'Reading Type' : 'string'}))
	#client2.publish("Data/F_Haus/Floor_1/Bedroom_A/Door", "Closed")
    #time.sleep(1)

    #client2.publish("Semantics/F_Haus/Floor_2/MasterBed/DeskLamp", json.dumps({'Description': 'Monitors the desk lamp', 'Reading Type' : 'string'}))
    #client2.publish("Data/F_Haus/Floor_2/MasterBed/DeskLamp", "ON")
    #time.sleep(0.5)

    #time.sleep(0.5)
    #client2.publish("Semantics/F_Haus/Floor_2/MasterBed/CeilingFan", json.dumps({'Description': 'Monitors the ceiling fan', 'Reading Type' : 'string'}))
	#client2.publish("Data/F_Haus/Floor_2/MasterBed/CeilingFan", "OFF")

    #time.sleep(0.5)
    #time.sleep(0.5)
    #client2.publish("Semantics/F_Haus/Floor_2/MasterBed/Heating", json.dumps({'Description': 'Monitors the heating', 'Reading Type' : 'string'}))
	#client2.publish("Data/F_Haus/Floor_2/MasterBed/Heating", "OFF")    #@#@
    #time.sleep(0.25)

    #time.sleep(0.25)
    #client2.publish("Semantics/F_Haus/Floor_3/Bedroom_A/Humidity", json.dumps({'Description': 'Monitors the humidity', 'Reading Type' : 'string'}))
	#client2.publish("Data/F_Haus/Floor_3/Bedroom_A/Humidity", "Normal")
    #time.sleep(0.25)

    
