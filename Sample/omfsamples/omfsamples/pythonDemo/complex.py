# Import packages
import sys, json, random, time, platform, requests, socket, datetime, random

relay_url = "http://localhost:8118/ingress/messages"
producer_token = "python-omf-1.0-rc3"

# ************************************************************************
def create_data_values_stream_message(target_stream_id):
    data_values_JSON = ''
    timestamp = datetime.datetime.utcnow().isoformat() + 'Z'
    reading4 = {'reading5': 100*random.random(), 'reading6': 100*random.random()}
    data_values_JSON = [
        {
            "containerid": target_stream_id,
            "values": [
                {
                    "Time": timestamp,
                    "reading1": 100*random.random(),
                    "reading2": 100*random.random(),
                    "reading3": [100*random.random(), 100*random.random(), 100*random.random()],
                    "reading4": reading4
                }
            ]
        }
    ]
    return data_values_JSON

# ************************************************************************
def sendOMFMessageToEndPoint(message_type, OMF_data):
        try: 
                msg_header = {'producertoken': producer_token, 'messagetype': message_type, 'action': 'create', 'messageformat': 'JSON', 'omfversion': '1.0'}   
                response = requests.post(relay_url, headers=msg_header, data=json.dumps(OMF_data), verify=False, timeout=30)
                print('Response from relay from the initial "{0}" message: {1} {2}'.format(message_type, response.status_code, response.text))
        except Exception as e:
                print(str(datetime.datetime.now()) + " An error ocurred during web request: " + str(e))		

# ************************************************************************

types = [
    {
        "id": "type_tank",
        "type": "object",
        "classification": "static",
        "properties": {
                "Name": {
                        "type": "string",
                        "isindex": True
                },
                "Location": {
                        "type": "string"
                },
                "array": {
                        "type": "array", "items": {"type": "integer"}, "maxItems": 3
                },
                "object": {
                        "type": "object",
                        "properties": {
                                "nested1": {"type": "integer"},
                                "nested2": {"type": "integer"}
                        }
				}
        }
	},
	{
        "id": "type_measurement",
        "type": "object",
        "classification": "dynamic",
        "properties": {
                "Time": {
                        "format": "date-time",
                        "type": "string",
                        "isindex": True
                },
                "reading1": {
                        "type": "number"
                },
                "reading2": {
                        "type": "number"
                },
                "reading3": {
                        "type": "array", "items": {"type": "integer"}, "maxItems": 3
                },
                "reading4": {
                        "type": "object",
                        "properties": {
                                "reading5": {"type": "integer"},
                                "reading6": {"type": "number"}
                        }
				}
        }
    }
]

containers = [{
        "id": "measurement1",
        "typeid": "type_measurement"
}]

staticData = [{
        "typeid": "type_tank",
        "values": [{
                "Name": "tank1",
                "Location": "New York",
                "array": [2, 5, 4],
                "object": {"nested1": 37, "nested2": 46}
        }]
}]

linkData = [{
        "typeid": "__Link",
        "values": [ {
                "source": {
                        "typeid": "type_tank",
                        "index": "_ROOT"
                },
                "target": {
                        "typeid": "type_tank",
                        "index": "tank1"
                }
        },{
                "source": {
                        "typeid": "type_tank",
                        "index": "tank1"
                },
                "target": {
                        "containerid": "measurement1"
                }
        }]
    }]

#dataValue = [{
#        "containerid": "measurement1",
#        "values": [{
#                "Time": "2017-01-11T22:23:23.430Z",
#                "reading1": "1.0",
#                "reading2": "2.0"
#        }]
#}]

requests.packages.urllib3.disable_warnings()

    
sendOMFMessageToEndPoint("Type", types)
#time.sleep(1)
sendOMFMessageToEndPoint("Container", containers)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", staticData)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", linkData)
# time.sleep(1)
# ************************************************************************
while True:
    values = create_data_values_stream_message("measurement1")
    sendOMFMessageToEndPoint("Data", values)
    time.sleep(1)
