# Import packages
import sys, json, random, time, platform, requests, socket, datetime, random

relay_url = "https://127.0.0.1:5462/ingress/messages"
producer_token = "uid=d62ff1d0-9a79-4e22-b958-d14237e56341&crt=20180710180329203&sig=+U4kWJQuIWsVxwybLoAyYmg7VHqEa4SquxbG0vNtXeA="

# ************************************************************************
def create_data_values_stream_message(target_stream_id):
    data_values_JSON = ''
    timestamp = datetime.datetime.utcnow().isoformat() + 'Z'
    data_values_JSON = [
        {
            "containerid": target_stream_id,
            "values": [
                {
                    "Time": timestamp,
                    "reading1": 100*random.random(),
                    "reading2": 100*random.random()
                }
            ]
        }
    ]
    return data_values_JSON

# ************************************************************************
def sendOMFMessageToEndPoint(message_type, OMF_data):
        try: 
                msg_header = {'producertoken': producer_token, 'messagetype': message_type, 'action': 'create', 'messageformat': 'JSON', 'omfversion': '1.0'}   
                response = requests.post(relay_url, headers=msg_header, data=json.dumps(OMF_data), verify=False, timeout=30)
                print('Response from relay from the initial "{0}" message: {1} {2}'.format(message_type, response.status_code, response.text))
        except Exception as e:
                print(str(datetime.datetime.now()) + " An error ocurred during web request: " + str(e))		

# ************************************************************************

types = [
    {
        "id": "type_tank",
        "type": "object",
        "classification": "static",
        "properties": {
                "Name": {
                        "type": "string",
                        "isindex": True
                },
                "Location": {
                        "type": "string"
                }
        }
	},
	{
        "id": "type_measurement",
        "type": "object",
        "classification": "dynamic",
        "properties": {
                "Time": {
                        "format": "date-time",
                        "type": "string",
                        "isindex": True
                },
                "reading1": {
                        "type": "number"
                },
                "reading2": {
                        "type": "number"
                }
        }
    }
]

containers = [{
        "id": "measurement1",
        "typeid": "type_measurement"
}]

staticData = [{
        "typeid": "type_tank",
        "values": [{
                "Name": "tank1",
                "Location": "New York"
        }]
}]

linkData = [{
        "typeid": "__Link",
        "values": [ {
                "source": {
                        "typeid": "type_tank",
                        "index": "_ROOT"
                },
                "target": {
                        "typeid": "type_tank",
                        "index": "tank1"
                }
        },{
                "source": {
                        "typeid": "type_tank",
                        "index": "tank1"
                },
                "target": {
                        "containerid": "measurement1"
                }
        }]
    }]

#dataValue = [{
#        "containerid": "measurement1",
#        "values": [{
#                "Time": "2017-01-11T22:23:23.430Z",
#                "reading1": "1.0",
#                "reading2": "2.0"
#        }]
#}]

requests.packages.urllib3.disable_warnings()

    
sendOMFMessageToEndPoint("Type", types)
#time.sleep(1)
sendOMFMessageToEndPoint("Container", containers)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", staticData)
# time.sleep(1)
sendOMFMessageToEndPoint("Data", linkData)
# time.sleep(1)
# ************************************************************************
while True:
    values = create_data_values_stream_message("measurement1")
    sendOMFMessageToEndPoint("Data", values)
    time.sleep(1)
