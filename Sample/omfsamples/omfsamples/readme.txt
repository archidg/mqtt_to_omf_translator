
PI Connector Relay with OSIsoft Message Format (OMF) support - beta
18th October 2017

References
http://omf-docs.osisoft.com - OMF 1.0 specification
https://pisquare.osisoft.com/groups/pi-relay - forum to download files, ask questions and discuss

Requirements
Download the following files from https://pisquare.osisoft.com/groups/pi-relay
1. PI Connector Relay with OMF support
2. PI Connector Relay manual
3. Samples

The steps below were tested on a clean build of Windows Server 2016 Standard (Desktop experience)

1. Install the PI Connector Relay
2. Configure the Relay using the Web Admin UI - see the manual for details
	Pointer:
	PI Connector Relay Web Admin UI:  https://<hostname>:8118/admin/ui
	Tasks:
	Configure the Servers the Relay will send data to.
3. See details below on how to run the samples

Examples - C
Extract the samples file and the cDemo sample
Note: when updating the Relay address use https *not* http
Review the pdf document in the cDemo.zip Doc directory.
Note the Reset Relay envioronment section below that may be required when experimenting with the Relay.

Examples - Python 
1. Extract omfsamples.zip and then the python example
2. Overview of python examples
2.1 sample.py - Modify the script updating the relay_url variable, e.g.:
    Note: https *not* http, i.e.:
    relay_url = "https://emer:8118/ingress/messages"
2.2 if necessary update the producer_token variable
	producer_token = "theToken"
2.2 If necessary install requests library
    pip3 install requests
2.3 Run the script
    python3 simple.py
Note: To run the complex.py sample after the simple.py sample the environment must be reset.

Reset Relay environment
When you define a type, the PI Connector Relay stores this definition. There are two ways to change or
update a type
a. update the type version number
b. delete the PI Connector Relay type cache.

Reset the PI Connector Relay cache
1. Stop the Relay
2. Delete the folder %programdata%\OSIsoft\Tau\OSIsoft.Tau.Relay.Host
3. Start the Relay.